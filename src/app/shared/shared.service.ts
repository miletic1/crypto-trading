import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SharedService {

  result:any;

  constructor(private _http: HttpClient) {}

  getNews() {
    return this._http.get("http://api.mediastack.com/v1/news?access_key=b8f0dd56fd60875e687e61ed533c62ae&categories=technology,-business")
    map((result: any) => this.result = result);
  }
}
