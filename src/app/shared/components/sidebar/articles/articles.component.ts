import { Component, OnInit } from '@angular/core';
import {SharedService} from "../../../shared.service";

@Component({
  selector: 'app-articles',
  templateUrl: './articles.component.html',
  styleUrls: ['./articles.component.scss']
})
export class ArticlesComponent implements OnInit {

  objectKeys = Object.keys;
  news: any;

  constructor(private articleServices: SharedService) { }

  ngOnInit(): void {
    this.articleServices.getNews()
      .subscribe((res: any) => {
        this.news = res;
        console.log(res);
      });
  }

}
