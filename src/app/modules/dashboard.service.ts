import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  result:any;

  constructor(private _http: HttpClient) {}

  getPrices() {
    return this._http.get("https://min-api.cryptocompare.com/data/pricemulti?fsyms=BTC,ETH,XRP,ADA,SOL,DOGE,DOT,TRX,AVAX,DAI,SHIB,LTC,MATIC,XMR,XLM,LINK,APE,XTZ,FIL,IOT&tsyms=USD")
      map((result: any) => this.result = result);
  }

}
